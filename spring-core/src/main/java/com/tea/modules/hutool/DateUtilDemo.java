package com.tea.modules.hutool;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.net.URLDecoder;

import java.nio.charset.Charset;
import java.util.Date;

/**
 * @author jaymin
 * @create 2023-05-05 15:28
 */
public class DateUtilDemo {
    public static void main(String[] args) {
        Date date = DateUtil.lastMonth().toJdkDate();
        Date endDate = DateUtil.nextMonth().toJdkDate();
        long days = DateUtil.between(date, endDate, DateUnit.DAY);
        System.out.println("昨天跟明天的差:" + days);

        System.out.println(URLDecoder.decode("%E6%B0%B4%E7%94%B5%E6%80%BB%E5%87%BA%E5%8A%9B2022%2F10%2F01%7E2022%2F10%2F31.xlsx", Charset.defaultCharset()));
    }
}
