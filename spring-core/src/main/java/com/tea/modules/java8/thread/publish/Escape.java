package com.tea.modules.java8.thread.publish;

import com.tea.modules.java8.thread.annotation.ThreadUnSafe;
import lombok.extern.slf4j.Slf4j;

/**
 * @author jaymin
 * @since 2022/3/3 23:43
 */
@Slf4j
@ThreadUnSafe
public class Escape {
    private int thisCanBeEscape = 0;

    public Escape() {
        new InnerClass();
    }

    private class InnerClass {
        /**
         * 对象未完成构造之前不可以将其发布
         */
        public InnerClass() {
            log.info("{}", Escape.this.thisCanBeEscape);
        }
    }

    public static void main(String[] args) {
        new Escape();
    }
}
