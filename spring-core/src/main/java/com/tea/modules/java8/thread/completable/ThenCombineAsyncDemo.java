package com.tea.modules.java8.thread.completable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * com.tea.modules.java8.thread.completable
 *
 * @author jaymin
 * @since 2022/4/15
 */
public class ThenCombineAsyncDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> a = CompletableFuture.completedFuture(30).thenApplyAsync(i -> {
            System.out.println("乐惠国际股价:" + i);
            return i;
        });
        CompletableFuture<Integer> b = CompletableFuture.completedFuture(40).thenApplyAsync(i -> {
            System.out.println("乐惠国际股价" + i);
            return i;
        });
        CompletableFuture<Integer> completableFuture = a.thenCombineAsync(b, (f1, f2) -> {
            System.out.println("我现在执行输出:" + f1 + "," + f2);
            return f1 + f2;
        });
        System.out.println(completableFuture.get());
        a.join();
        b.join();
    }
}
