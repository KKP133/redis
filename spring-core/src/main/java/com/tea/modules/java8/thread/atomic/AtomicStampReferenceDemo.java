package com.tea.modules.java8.thread.atomic;

import com.tea.modules.java8.thread.annotation.ThreadSafe;
import lombok.extern.slf4j.Slf4j;

/**
 * 解决CAS的ABA问题
 * @author jaymin
 * @since 2022/2/22 1:08
 */
@ThreadSafe
@Slf4j
public class AtomicStampReferenceDemo {

    public static void main(String[] args) {

    }
}
