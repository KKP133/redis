package com.tea.modules.java8.collection.lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * com.tea.modules.java8.collection.lists
 * 集合的视图操作
 * @author jaymin
 * @since 2022/4/13
 */
public class CollectionsViewDemo {

    public static void main(String[] args) {
//        asList();
//        singleton();
//        empty();
//        subList();

    }

    private static void subList() {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        List<Integer> subList = numbers.subList(0, 3);
        subList.forEach(System.out::println);
        subList.add(22);
        subList.forEach(System.out::println);
        System.out.println("numbers中的元素:");
        numbers.forEach(System.out::println);
        subList.clear();
        System.out.println("numbers中的元素:");
        numbers.forEach(System.out::println);
    }

    private static void empty() {
        List<String> emptyList = Collections.emptyList();
        emptyList.add("null");
    }

    private static void singleton() {
        List<Integer> one = Collections.singletonList(1);
        one.add(2);
    }


    private static void asList() {
        List<Integer> numbers = Arrays.asList(1, 2, 3);
        numbers.add(4);
    }
}
