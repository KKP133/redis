package com.tea.modules.java8.reflect;

import com.google.common.collect.Maps;
import com.tea.modules.model.po.Student;
import net.sf.cglib.beans.BeanMap;

import java.util.Date;
import java.util.HashMap;

/**
 * com.tea.modules.java8.reflect
 * 使用cglib的反射进行对象转换
 * @author jaymin
 * @since 2023/3/14
 */
public class BeanMapDemo {

    public static void main(String[] args) {
        Student student = new Student();
        HashMap<String, Object> objMap = Maps.newHashMap();
        BeanMap beanMap = BeanMap.create(student);
        objMap.put("age",1);
        objMap.put("birthDay",new Date());
        objMap.put("name","CGLIB");
        for (Object key : objMap.keySet()) {
            String propertyName = (String) key;
            Object propertyValue = objMap.get(propertyName);
            beanMap.put(propertyName, propertyValue);
        }
        System.out.println(student);
    }
}
