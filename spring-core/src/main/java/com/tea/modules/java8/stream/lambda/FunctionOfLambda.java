package com.tea.modules.java8.stream.lambda;

import com.tea.modules.model.po.User;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * com.tea.modules.java8.stream.lambda
 *
 * @author jaymin
 * @since 2022/1/13
 */
public class FunctionOfLambda {
    public static void main(String[] args) {
        Function<Integer,String> function = new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return String.valueOf(integer);
            }
        };
        String result = function.apply(1);
        System.out.println(result);
        Function<Integer,String> functionA = integer -> String.valueOf(integer);

        Supplier<User> userSupplier = () -> {
            return new User();
        };
    }

}
