package com.tea.modules.java8.collection.maps;

import java.util.HashMap;
import java.util.Map;

/**
 * com.tea.modules.java8.collection.maps <br>
 * 用于测试Map的API
 * @author jaymin
 * @since 2021/9/18
 */
public class WhatIsMap {

    public static void main(String[] args) {
        whatIsHashMap();
    }

    /**
     * 演示hashMap最简单的用法
     */
    private static void whatIsHashMap() {
        Map<String,Object> hashMap = new HashMap<>();
        hashMap.put("男","man");
        hashMap.put("女","woman");
        System.out.println(hashMap.get("男"));
    }
}
