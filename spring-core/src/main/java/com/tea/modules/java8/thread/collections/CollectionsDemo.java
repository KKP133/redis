package com.tea.modules.java8.thread.collections;

import com.tea.modules.java8.thread.annotation.ThreadUnSafe;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author jaymin
 * @since 2022/3/6 0:29
 */
@ThreadUnSafe
@Slf4j
public class CollectionsDemo {

    /**
     * 请求总数
     */
    public static final int clientTotal = 5000;
    /**
     * 同时并发线程数
     */
    public static final int threadTotal = 200;
    public static List<Integer> list = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal; i++) {
            int addI = i;
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    add(addI);
                    semaphore.release();
                } catch (InterruptedException | ParseException e) {
                    log.error("并发错误:", e);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        log.info("size:{}", list.size());
    }

    /**
     * 线程不安全
     * @param i
     * @throws ParseException
     */
    private static void add(int i) throws ParseException {
        list.add(i);
    }
}
