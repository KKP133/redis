package com.tea.modules.java8.encoder;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * com.tea.modules.java8.encoder
 *
 * @author jaymin
 * @since 2021/9/27
 */
public class URLEncoderDemo {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String result = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=ww9f7a30929e721767&redirect_uri=%s&response_type=code&scope=snsapi_base&state=#wechat_redirect;";
        String encode = URLEncoder.encode("https://test-mall-wechatwork.int.partschance.com/api/wechatwork/bs/wx/work/redirect", "UTF-8");
        System.out.println(encode);
        System.out.println(String.format(result,encode));
    }
}
