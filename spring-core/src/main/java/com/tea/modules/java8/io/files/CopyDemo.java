package com.tea.modules.java8.io.files;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * com.tea.modules.java8.io.files
 *
 * @author jaymin
 * @since 2022/9/21
 */
public class CopyDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\jaymin\\Desktop\\stdout.log.txt");
        File file1 = new File("C:\\Users\\jaymin\\Desktop\\stdout.log1.txt");
        FileUtils.copyFile(file,file1);
    }
}
