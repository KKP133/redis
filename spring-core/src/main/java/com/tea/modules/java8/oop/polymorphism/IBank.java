package com.tea.modules.java8.oop.polymorphism;

/**
 * com.tea.modules.java8.oop.polymorphism <br>
 * 银行接口
 * @author jaymin
 * @since 2021/7/1
 */
public interface IBank {
    String EXAMPLE = "我是一个例子，说明接口是可以定义属性的";
    /**
     * 办理信用卡
     */
    void applyCreditCard();
}
