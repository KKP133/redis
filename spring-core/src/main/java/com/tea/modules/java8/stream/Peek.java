package com.tea.modules.java8.stream;

import com.tea.modules.model.po.StudentForLambda;

import java.util.Arrays;
import java.util.List;

/**
 * com.tea.modules.java8.stream
 * API注意事项：
 * 此方法的存在主要是为了支持调试，您希望在其中查看元素流经管道中的某个点时的情况：
 * 区别于foreach是，它是中间层的操作
 * @author jaymin
 * @since 2021/8/26
 */
public class Peek {
    public static void main(String[] args) {
        StudentForLambda studentA = new StudentForLambda(1L,"特朗普",45,"漂亮国");
        StudentForLambda studentB = new StudentForLambda(2L,"大雄",30,"日本");
        StudentForLambda studentC = new StudentForLambda(3L,"拿破仑",60,"法国");
        StudentForLambda studentD = new StudentForLambda(4L,"孙中山",25,"中国");
        List<StudentForLambda> studentList = Arrays.asList(studentA,studentB,studentC,studentD);
        long count = studentList.stream()
                .peek(student -> System.out.println(student.getName()))
                .count();
    }
}
