package com.tea.modules.java8.thread.watch;

/**
 * 用来验证多线程是随机执行的 <br>
 * 注意，start才是真正创建线程去执行的，而不是run. <br>
 * 多线程随机输出的原因是CPU将时间片分给不同的线程，线程获取时间片后执行任务<br>
 * 在这个过程中，线程交替执行任务. <br>
 * 多线程在切换的过程中需要切换线程上下文，所以开启多个线程并不一定会让执行效率高. <br>
 * @author jaymin
 * @since 2021/9/4 0:34
 */
public class RandomExecute {

    public static void main(String[] args) {
        Runnable runnable = () -> {
            printCurrentThreadName();
        };
        Thread thread = new Thread(runnable);
        thread.setName("随机线程");
        thread.start();
        printCurrentThreadName();
    }

    private static void printCurrentThreadName() {
        for (int i = 0; i < 10000; i++) {
            System.out.println("run=" + Thread.currentThread().getName());
        }
    }
}
