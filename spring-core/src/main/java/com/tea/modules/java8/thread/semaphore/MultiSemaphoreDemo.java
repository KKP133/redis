package com.tea.modules.java8.thread.semaphore;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 多个许可的信号量控制
 * @author jaymin
 * @since 2022/3/7 0:49
 */
@Slf4j
public class MultiSemaphoreDemo {

    public static void main(String[] args) throws InterruptedException {
        int state = 2;
        final Semaphore semaphore = new Semaphore(6);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 20; i++) {
            executorService.execute(()->{
                try {
                    // 获取3个许可
                    semaphore.acquire(3);
                    doSomeThing();
                    // 释放3个许可
                    semaphore.release(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();

    }

    private static void doSomeThing() {
        log.info("record");
    }
}
