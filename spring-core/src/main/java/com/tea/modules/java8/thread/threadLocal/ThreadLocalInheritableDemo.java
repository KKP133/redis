package com.tea.modules.java8.thread.threadLocal;

/**
 * com.tea.modules.java8.thread.threadLocal
 *
 * @author jaymin
 * @since 2022/8/30
 */
public class ThreadLocalInheritableDemo {

    public static InheritableThreadLocal<String> threadLocal = new InheritableThreadLocal<>();

    public static void main(String[] args) {
        threadLocal.set("hello world");
        new Thread(() -> {
            System.out.println("thread:" + threadLocal.get());
        }).start();
        System.out.println("main:" + threadLocal.get());
    }
}
