package com.tea.modules.java8.thread.future;

import java.util.concurrent.TimeUnit;

/**
 * com.tea.modules.java8.thread.future <br>
 * nap-打屯
 * @author jaymin
 * @since 2022/4/14
 */
public class Nap {
    /**
     * 停留T秒
     * @param t
     */
    public Nap(double t) {
        try {
            TimeUnit.MILLISECONDS.sleep((int) (1000 * t));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public Nap(double t, String msg) {
        this(t);
        System.out.println(msg);
    }
}
