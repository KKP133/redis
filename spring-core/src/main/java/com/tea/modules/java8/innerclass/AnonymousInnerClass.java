package com.tea.modules.java8.innerclass;

/**
 * com.tea.modules.java8.innerclass <br>
 * 匿名内部类
 *
 * @author jaymin
 * @since 2021/9/6
 */
interface Inner {
    int value();
}

public class AnonymousInnerClass {
    public Inner innerClass() {
        return new Inner() {
            private int i;

            @Override
            public int value() {
                return ++i;
            }
        };
    }

    public static void main(String[] args) {
        System.out.println(new AnonymousInnerClass().innerClass().value());
    }
}
