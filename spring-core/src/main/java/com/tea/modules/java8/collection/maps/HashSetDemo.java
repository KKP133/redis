package com.tea.modules.java8.collection.maps;

import java.util.HashSet;

/**
 * com.tea.modules.java8.collection.maps
 * 集合
 *
 * @author jaymin
 * @since 2022/4/12
 */
public class HashSetDemo {

    public static void main(String[] args) {
        HashSet<Integer> hashSet = new HashSet<>();
        hashSet.add(1);
        hashSet.add(1);
        hashSet.add(1);
        hashSet.add(1);
        System.out.println("当前集合中的元素个数为:" + hashSet.size());
    }
}
