package com.tea.modules.java8.spi;

import java.util.ServiceLoader;

/**
 * com.tea.modules.java8.spi
 *
 * @author jaymin
 * @since 2022/4/13
 */
public class SPIDemo {
    public static void main(String[] args) {
        ServiceLoader<ICipher> ciphers = ServiceLoader.load(ICipher.class);
        ciphers.forEach(iCipher -> {
            iCipher.encrypt(null,null);
        });
    }
}
