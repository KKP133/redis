package com.tea.modules.java8.thread.date;

import com.tea.modules.java8.thread.annotation.ThreadUnSafe;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author jaymin
 * @since 2022/3/6 0:18
 */
@ThreadUnSafe
@Slf4j
public class SimpleDateFormatDemo {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    /**
     * 请求总数
     */
    public static final int clientTotal = 5000;
    /**
     * 同时并发线程数
     */
    public static final int threadTotal = 200;

    public static AtomicInteger count = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal; i++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    add();
                    semaphore.release();
                } catch (InterruptedException | ParseException e) {
                    log.error("并发错误:", e);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
    }

    private static void add() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.parse("20180218");
    }
}
