package com.tea.modules.java8.thread.completable;

import com.tea.modules.java8.thread.future.Nap;
import com.tea.modules.model.po.Baked;

import java.util.concurrent.CompletableFuture;

/**
 * com.tea.modules.java8.thread.completable
 *
 * @author jaymin
 * @since 2022/4/24
 */

final class Frosting {
    private Frosting() {
    }

    static CompletableFuture<Frosting> make() {
        new Nap(0.1);
        return CompletableFuture
                .completedFuture(new Frosting());
    }
}


