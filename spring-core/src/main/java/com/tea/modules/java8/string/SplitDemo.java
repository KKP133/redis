package com.tea.modules.java8.string;

/**
 * com.tea.modules.java8.string
 *
 * @author jaymin
 * @since 2022/5/6
 */
public class SplitDemo {
    public static void main(String[] args) {
        String pf = "2022-05-06我是谁";
        String style = "2022-05-06";
        String fileName = pf.substring(style.length());
        System.out.println(fileName);
    }
}
