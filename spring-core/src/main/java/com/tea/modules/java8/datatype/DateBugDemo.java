package com.tea.modules.java8.datatype;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.tea.modules.model.po.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * com.tea.modules.java8.datatype
 *
 * @author jaymin
 * @since 2022/12/8
 */
public class DateBugDemo {
    public static void main(String[] args) {
        Student student = new Student();
        student.setBirthDay(DateUtil.date(1641052800000L));
        student.setName("我是BUG日期");
        Student studentA = new Student();
        studentA.setBirthDay(DateUtil.date(1640880000000L));
        studentA.setName("我是BUG日期1");
        ArrayList<Student> students = Lists.newArrayList(student, studentA);
        List<Student> studentList = students.stream().map(s -> {
            s.setBirthDay(s.getBirthDay());
            return s;
        }).collect(Collectors.toList());
        System.out.println(JSONUtil.toJsonPrettyStr(studentList));
    }
}
