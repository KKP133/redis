package com.tea.modules.java8.thread.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jaymin
 * @since 2022/2/19 15:34
 */
@RestController("TT")
@Slf4j
@RequestMapping("/thread/test")
public class TestController {

    @GetMapping("")
    public String test() {
        return "OK";
    }
}
