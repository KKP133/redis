package com.tea.modules.java8.thread.publish;

/**
 * @author jaymin
 * @since 2022/3/3 23:54
 */
public class SafePublish {
    private SafePublish() {
    }

    /**
     * 单例对象
     */
    private static volatile SafePublish instance = null;

    /**
     * 静态的工厂方法
     *
     * @return
     */
    public static SafePublish getInstance() {
        if (instance == null) {
            synchronized (SafePublish.class) {
                if (instance == null) {
                    instance = new SafePublish();
                }
            }
        }
        return instance;
    }
}
