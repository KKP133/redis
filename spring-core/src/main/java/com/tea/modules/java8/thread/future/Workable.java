package com.tea.modules.java8.thread.future;

import java.util.concurrent.CompletableFuture;

/**
 * com.tea.modules.java8.thread.future
 *
 * @author jaymin
 * @since 2022/4/15
 */
public class Workable {
    String id;
    final double duration;

    public Workable(String id, double duration) {
        this.id = id;
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Workable[" + id + "]";
    }

    /**
     * 输出对象信息
     * @param tt
     * @return
     */
    public static Workable work(Workable tt) {
        // Seconds
        new Nap(tt.duration);
        tt.id = tt.id + "W"+Thread.currentThread().getName();
        System.out.println(tt);
        return tt;
    }

    /**
     * 执行work
     * @param id
     * @param duration
     * @return
     */
    public static CompletableFuture<Workable> make(String id, double duration) {
        return CompletableFuture.completedFuture(new Workable(id, duration))
                .thenApplyAsync(Workable::work);
    }
}
