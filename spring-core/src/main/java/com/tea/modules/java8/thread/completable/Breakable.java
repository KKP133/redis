package com.tea.modules.java8.thread.completable;

/**
 * com.tea.modules.java8.thread.completable
 *与 CompletableFuture 在处理链中包装对象的方式相同，它也会缓冲异常。
 * 这些在处理时调用者是无感的，但仅当你尝试提取结果时才会被告知。
 * 为了说明它们是如何工作的，我们首先创建一个类，它在特定的条件下抛出一个异常
 * @author jaymin
 * @since 2022/4/24
 */
public class Breakable {
    String id;
    private int failCount;

    public Breakable(String id, int failCount) {
        this.id = id;
        this.failCount = failCount;
    }

    @Override
    public String toString() {
        return "Breakable_" + id + " [" + failCount + "]";
    }

    /**
     * 当failcount > 0，且每次将对象传递给 work() 方法时， failcount - 1 。 <br>
     * 当failcount - 1 = 0 时，work() 将抛出一个异常。 <br>
     * 如果传给 work() 的 failcount = 0 ，work() 永远不会抛出异常。 <br>
     *
     * 注意，异常信息此示例中被抛出（ RuntimeException )
     * @param b
     * @return
     */
    public static Breakable work(Breakable b) {
        if (--b.failCount == 0) {
            System.out.println("假异常: " + b.id + "");
            throw new RuntimeException(
                    "真异常-" + b.id + ""
            );
        }
        System.out.println(b);
        return b;
    }
}
