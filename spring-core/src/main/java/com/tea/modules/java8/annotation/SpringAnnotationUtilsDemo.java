package com.tea.modules.java8.annotation;

import com.tea.modules.model.po.DataType;
import com.tea.modules.model.po.ReadObj;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

/**
 * @author jaymin
 * @create 2023-05-15 16:02
 */
public class SpringAnnotationUtilsDemo {

    public static void main(String[] args) {
        ReflectionUtils.doWithFields(ReadObj.class,field -> {
            DataType dataType = AnnotationUtils.getAnnotation(field, DataType.class);
            System.out.println(dataType);
        });
    }
}
