package com.tea.modules.java8.thread.watch;

/**
 * 如何查看线程的状态，我们这里通过死循环去调用多线程，然后执行JVM指令: <br>
 * jps+jstack来查看线程状态<br>
 * $> jps             <br>
 * $> jstack -l pid   <br>
 * @author jaymin
 * @since 2021/9/4 0:29
 */
public class Running {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            Runnable runnable = ()->{
                try {
                    Thread.sleep(500000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            new Thread(runnable).start();
        }
    }
}
