package com.tea.modules.java8.thread.completable;

import com.tea.modules.model.po.User;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * com.tea.modules.java8.thread.completable
 *
 * @author jaymin
 * @since 2022/4/29
 */
public class CompletableDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        User user = User.builder().age(1).userName("张三").build();
        CompletableFuture<User> completableFuture = CompletableFuture.completedFuture(user)
                .thenApplyAsync(u -> {
                    System.out.println("保存信息");
                    return u;
                })
                .thenApplyAsync(u -> {
                    System.out.println("现在向" + u.getUserName() + "发送信息");
                    return u;
                });
        completableFuture.get();
        completableFuture.join();
    }
}
