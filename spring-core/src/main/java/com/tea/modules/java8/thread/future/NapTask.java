package com.tea.modules.java8.thread.future;

/**
 * com.tea.modules.java8.thread.future
 *
 * @author jaymin
 * @since 2022/4/14
 */
public class NapTask implements Runnable {
    final int id;

    public NapTask(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        new Nap(0.1);// Seconds
        System.out.println(this + " " +
                Thread.currentThread().getName());
    }

    @Override
    public String toString() {
        return "NapTask[" + id + "]";
    }
}