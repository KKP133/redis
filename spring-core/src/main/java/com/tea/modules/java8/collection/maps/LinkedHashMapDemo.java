package com.tea.modules.java8.collection.maps;

import java.util.LinkedHashMap;

/**
 * com.tea.modules.java8.collection.maps
 *
 * @author jaymin
 * @since 2022/4/13
 */
public class LinkedHashMapDemo {

    public static void main(String[] args) {
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("我是第一个", "111");
        linkedHashMap.put("我是第二个", "222");
        linkedHashMap.put("我是第三个", "333");
        linkedHashMap.forEach((k, v) -> System.out.println(k + ":" + v));
    }
}
