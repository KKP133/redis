package com.tea.modules.java8.collection.maps;

import java.util.TreeMap;

/**
 * com.tea.modules.java8.collection.maps
 *
 * @author jaymin
 * @since 2022/4/12
 */
public class TreeMapDemo {

    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(1, "我是1");
        treeMap.put(3, "我是3");
        treeMap.put(2, "我是2");
        treeMap.forEach((k, v) -> System.out.println(k + "," + v));
    }
}
