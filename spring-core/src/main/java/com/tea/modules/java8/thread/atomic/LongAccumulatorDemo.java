package com.tea.modules.java8.thread.atomic;

import java.util.concurrent.atomic.LongAccumulator;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.LongBinaryOperator;

/**
 * com.tea.modules.java8.thread.atomic
 * LongAccumulator学习
 *
 * @author jaymin
 * @since 2022/11/7
 */
public class LongAccumulatorDemo {
    public static void main(String[] args) {
        // left代表base的值，也就是1；
        // right代表的是传进来的值，也就下面accumulate函数传的值
        LongAccumulator longAccumulator = new LongAccumulator((left, right) -> left * right, 1);
        longAccumulator.accumulate(2);
        System.out.println(longAccumulator.get());

        LongAdder longAdder = new LongAdder();
        longAdder.add(1);
        System.out.println(longAdder.longValue());

        LongAccumulator longAdd = new LongAccumulator(Long::sum, 0);
        longAdd.accumulate(1);
        System.out.println(longAdd.get());
    }
}
