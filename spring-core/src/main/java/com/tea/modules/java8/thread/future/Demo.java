package com.tea.modules.java8.thread.future;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * com.tea.modules.java8.thread.future
 *
 * @author jaymin
 * @since 2022/3/10
 */
public class Demo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ArrayList<Integer> integers = Lists.newArrayList(1, 2, 3);
        CompletableFuture<String> completableFutureA = CompletableFuture.supplyAsync(() -> doS(1));
        CompletableFuture<String> completableFutureB = CompletableFuture.supplyAsync(() -> doS(2));
        CompletableFuture<String> completableFuture = completableFutureB.thenCombine(completableFutureA, (AResult, BResult) -> {
            System.out.println("A的结果是:" + AResult);
            System.out.println("B的结果是:" + BResult);
            return AResult + BResult;
        });
        String result = completableFuture.get();
        System.out.println("result:" + result);
    }

    private static String doS(Integer integer) {
        return "xxx" + integer;
    }
}
