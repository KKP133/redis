package com.tea.modules.java8.collection.lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * com.tea.modules.java8.collection.lists <br>
 * 用来分析List源码
 * @author jaymin
 * @since 2021/11/26
 */
public class CodeDemo {

    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        List<Integer> subList = list.subList(0, 2);
        subList.add(4);
        subList.remove(1);
        List<Integer> integers = Arrays.asList(1, 2);
        integers.add(2);
        subList.forEach(System.out::println);
    }

}
