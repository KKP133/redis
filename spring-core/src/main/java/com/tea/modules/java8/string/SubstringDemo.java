package com.tea.modules.java8.string;

/**
 * com.tea.modules.java8.string
 *
 * @author jaymin
 * @since 2021/11/2
 */
public class SubstringDemo {
    public static void main(String[] args) {
        String phone = "13800138000";
        System.out.println(phone.substring(phone.length() - 6));
    }
}
