package com.tea.modules.java8.oop.polymorphism;

/**
 * com.tea.modules.java8.oop.polymorphism <br>
 * 证券接口
 * @author jaymin
 * @since 2021/8/20
 */
public interface Securities {
    /**
     * 开通证券账户
     */
    void openSecurities();

    /**
     * 提醒散户跑路
     */
    static void prompt(){
        System.out.println("崩盘了，速度清仓!!!");
    }
}
