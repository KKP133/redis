package com.tea.modules.java8.thread.atomic;

import com.tea.modules.java8.thread.annotation.ThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author jaymin
 * @since 2022/2/19 21:58
 */
@ThreadSafe
@Slf4j
public class AtomicReferenceDemo {

    private static AtomicReference count = new AtomicReference<>(0);

    public static void main(String[] args) {
        // 是0的时候更新成2
        count.compareAndSet(0, 2);
        // 不执行
        count.compareAndSet(0, 1);
        // 不执行
        count.compareAndSet(1, 3);
        count.compareAndSet(2, 4);
        // 不执行
        count.compareAndSet(3, 5);
        System.err.println(count.get());
    }
}
