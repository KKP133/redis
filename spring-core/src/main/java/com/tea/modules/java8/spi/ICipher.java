package com.tea.modules.java8.spi;

/**
 * com.tea.modules.java8.spi
 * 定义一个加密服务
 * @author jaymin
 * @since 2022/4/13
 */
public interface ICipher {
    /**
     * 加密
     * @param source
     * @param key
     * @return
     */
    byte[] encrypt(byte[] source,byte[] key);
}
