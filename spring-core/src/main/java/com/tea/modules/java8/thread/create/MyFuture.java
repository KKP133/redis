package com.tea.modules.java8.thread.create;

import java.util.concurrent.*;

/**
 * com.tea.modules.java8.thread.create
 *
 * @author jaymin
 * @since 2022/4/29
 */
public class MyFuture implements Callable<String> {
    @Override
    public String call() throws Exception {
        System.out.println(System.currentTimeMillis() + ":发送短信");
        return "success";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println(System.currentTimeMillis() + ":保存用户信息");
        MyFuture myFuture = new MyFuture();
        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<String> future = executorService.submit(myFuture);
        String result = future.get();
        System.out.println(result);
        executorService.shutdown();
    }
}
