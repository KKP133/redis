package com.tea.modules.java8.thread.completable;

import com.tea.modules.java8.thread.future.Machina;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * com.tea.modules.java8.thread.completable
 *
 * @author jaymin
 * @since 2022/4/15
 */
public class CompletedMachina {
    public static void main(String[] args) {
        CompletableFuture<Machina> cf = CompletableFuture.completedFuture(new Machina(0))
                .thenApply(Machina::work)
                .thenApply(Machina::work)
                .thenApply(Machina::work)
                .thenApply(Machina::work);

        CompletableFuture<Machina> cfA = CompletableFuture.completedFuture(new Machina(0))
                .thenApplyAsync(Machina::work)
                .thenApplyAsync(Machina::work)
                .thenApplyAsync(Machina::work)
                .thenApplyAsync(Machina::work);
        // 事实上，如果没有回调 cf.join() 方法，程序会在完成其工作之前退出。
        // 而 cf.join() 直到 cf 操作完成之前，阻止 main() 进程结束。
        // 我们还可以看出本示例大部分时间消耗在 cf.join() 这。
        cfA.join();
    }
}
