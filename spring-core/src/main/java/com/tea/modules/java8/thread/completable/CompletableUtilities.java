package com.tea.modules.java8.thread.completable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * com.tea.modules.java8.thread.completable
 *
 * @author jaymin
 * @since 2022/4/15
 */
public class CompletableUtilities {

    /**
     *     Get and show value stored in a CF:
      */
    public static void showResult(CompletableFuture<?> c) {
        try {
            System.out.println(c.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * For CF operations that have no value:
     */
    public static void voidResult(CompletableFuture<Void> c) {
        try {
            c.get();
        } catch (InterruptedException
                | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
