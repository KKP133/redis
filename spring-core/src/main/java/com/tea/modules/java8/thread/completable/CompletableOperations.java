package com.tea.modules.java8.thread.completable;

import java.util.concurrent.CompletableFuture;
import static com.tea.modules.java8.thread.completable.CompletableUtilities.*;
/**
 * com.tea.modules.java8.thread.completable
 *
 * @author jaymin
 * @since 2022/4/15
 */
@SuppressWarnings("all")
public class CompletableOperations {
    static CompletableFuture<Integer> cfi(int i) {
        return CompletableFuture.completedFuture(i);
    }

    public static void main(String[] args) {
        showResult(cfi(1));
        // 无返回值,静态调用
        voidResult(cfi(2).runAsync(() -> System.out.println("runAsync")));
        // 无返回值，可以链式调用
        // thenApplyAsync() 接收一个Function, 并生成一个结果（该结果的类型可以不同于其输入类型）。
        voidResult(cfi(3).thenRunAsync(() -> System.out.println("thenRunAsync")));
        // 静态调用
        voidResult(CompletableFuture.runAsync(() -> System.out.println("runAsync is static")));
        // T->T
        showResult(CompletableFuture.supplyAsync(() -> 99));
        voidResult(cfi(4).thenAcceptAsync(i -> System.out.println("thenAcceptAsync: " + i)));
        showResult(cfi(5).thenApplyAsync(i -> i + 42));
        // thenComposeAsync() 与 thenApplyAsync()非常相似，唯一区别在于其 Function 必须产生已经包装在CompletableFuture中的结果。
        showResult(cfi(6).thenComposeAsync(i -> cfi(i + 99)));
        CompletableFuture<Integer> c = cfi(7);
        // cfi(7) 示例演示了 obtrudeValue()，它强制将值作为结果。
        c.obtrudeValue(111);
        showResult(c);
        // cfi(8) 使用 toCompletableFuture() 从 CompletionStage 生成一个CompletableFuture。
        showResult(cfi(8).toCompletableFuture());
        c = new CompletableFuture<>();
        // c.complete(9) 显示了如何通过给它一个结果来完成一个task（future）
        // （与 obtrudeValue() 相对，后者可能会迫使其结果替换该结果）。
        c.complete(9);
        showResult(c);
        c = new CompletableFuture<>();
        // 如果你调用 CompletableFuture中的 cancel()方法，如果已经完成此任务，则正常结束。
        // 如果尚未完成，则使用 CancellationException 完成此 CompletableFuture。
        c.cancel(true);
        System.out.println("cancelled: " +
                c.isCancelled());
        System.out.println("completed exceptionally: " +
                c.isCompletedExceptionally());
        System.out.println("done: " + c.isDone());
        System.out.println(c);
        c = new CompletableFuture<>();
        // 如果任务（future）完成，则 getNow() 方法返回CompletableFuture的完成值，否则返回getNow()的替换参数。
        System.out.println(c.getNow(777));
        c = new CompletableFuture<>();
        c.thenApplyAsync(i -> i + 42)
                .thenApplyAsync(i -> i * 12);
        System.out.println("dependents: " +
                c.getNumberOfDependents());
        c.thenApplyAsync(i -> i / 2);
        System.out.println("dependents: " +
                c.getNumberOfDependents());
    }
}
