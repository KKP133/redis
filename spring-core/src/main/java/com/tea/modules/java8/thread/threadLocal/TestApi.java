package com.tea.modules.java8.thread.threadLocal;

public class TestApi {
    //ThreadLocal<T>
    public static ThreadLocal<Long> x = ThreadLocal.withInitial(() -> {
        // 延迟加载，只在第一次get的时候进行初始化
        System.out.println(Thread.currentThread().getId() + "initialValue run...");
        return Thread.currentThread().getId();
    });

    public static void main(String[] args) {
        x.get();
        // ThreadLocal为每一个线程存储一个独立的变量
        new Thread(() -> {
            x.set(107L);
            System.out.println(x.get());
        }).start();
        // ThreadLocal为每一个线程存储一个独立的变量
        new Thread(() -> {
            x.set(108L);
            System.out.println(x.get());
        }).start();
        // 清空当前线程的ThreadLocal的值
        x.remove();
    }
}
