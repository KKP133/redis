package com.tea.modules.java8.thread.countdownlatch;

import lombok.extern.slf4j.Slf4j;

import java.util.Timer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author jaymin
 * @since 2022/3/7 0:25
 */
@Slf4j
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException {
        int state = 2;
        CountDownLatch countDownLatch = new CountDownLatch(state);
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(()->{
            log.info("我是1号员工，我作业完毕");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            countDownLatch.countDown();
        });
        executorService.execute(()->{
            log.info("我是2号员工，我作业完毕");
            countDownLatch.countDown();
        });
        countDownLatch.await(10, TimeUnit.SECONDS);
        log.info("OK，所有人工作完毕");
    }
}
