package com.tea.modules.java8.thread.create;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * com.tea.modules.java8.thread.create
 *
 * @author jaymin
 * @since 2022/4/29
 */
public class MyFutureTask {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyFuture myFuture = new MyFuture();
        FutureTask<String> futureTask = new FutureTask<>(myFuture);
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(futureTask);
        String result = futureTask.get();
        System.out.println("callable:" + result);
        MyRunnable myRunnable = new MyRunnable();
        FutureTask<String> runnableTask = new FutureTask<>(myRunnable, "success");
        executorService.submit(runnableTask);
        System.out.println("runnable:" + runnableTask.get());
        executorService.shutdown();
    }
}
