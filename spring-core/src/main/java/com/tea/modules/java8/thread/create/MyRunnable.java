package com.tea.modules.java8.thread.create;

/**
 * 实现Runnable接口来实现多线程 <br>
 * 使用Runnable更加好，因为JAVA语言不支持多继承，使用接口可以让扩展性更加好. <br>
 *
 * @author jaymin
 * @since 2021/9/4 0:40
 */
public class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println(System.currentTimeMillis() + ":发送信息");
        throw new RuntimeException("我其实报错了");
    }

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                //t参数接收发生异常的线程, e就是该线程中的异常
                System.out.println(t.getName() + "线程产生了异常: " + e.getMessage());
            }
        });
        System.out.println(System.currentTimeMillis() + ":保存用户信息");
        try {
            MyRunnable myRunnable = new MyRunnable();
            new Thread(myRunnable).start();
        } catch (Exception e) {
            System.out.println("线程产生了异常: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
