package com.tea.modules.java8.thread.completable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * com.tea.modules.java8.thread.completable
 *
 * @author jaymin
 * @since 2022/4/15
 */
public class AcceptEitherDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        for (int i = 0; i < 100; i++) {
            // 开市
            run();
        }
    }

    private static void run() throws InterruptedException, ExecutionException {
        CompletableFuture<Integer> a = CompletableFuture.completedFuture(30).thenApplyAsync(i -> {
            System.out.println("乐惠国际股价:" + i);
            return i;
        });
        CompletableFuture<Integer> b = CompletableFuture.completedFuture(40).thenApplyAsync(i->{
            System.out.println("乐惠国际股价" + i);
            return i;
        });
        CompletableFuture<Void> completableFuture = a.acceptEither(b, i -> {
            System.out.println("我现在输出乐惠国际股价:"+i);
        });
        completableFuture.get();
        a.join();
        b.join();
    }
}
