package com.tea.modules.java8.thread.create;

/**
 * 使用继承的方式实现多线程 <br>
 * 1. 通过JVM告诉操作系统创建线程 <br>
 * 2. 操作系统开辟内存并根据不同平台的创建线程函数创建线程对象 <br>
 * 3. 操作系统对线程对象进行调度，以确定执行时机 <br>
 * 4. 线程任务被执行,注意，异步执行并不保证先后顺序
 *
 * @author jaymin
 * @since 2021/9/4 0:21
 */
public class MyThread extends Thread {

    @Override
    public void run() {
        super.run();
        System.out.println(System.currentTimeMillis() + ":发送信息:" + Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis() + ":保存用户信息");
        MyThread myThread = new MyThread();
        // 注意，启动多线程用start而不是run
        // 调用start方法最终会调用run方法
        myThread.start();
    }
}
