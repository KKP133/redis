package com.tea.modules.java8.thread.immutable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.tea.modules.java8.thread.annotation.ThreadSafe;

import java.util.Collections;
import java.util.Map;

/**
 * @author jaymin
 * @since 2022/3/5 20:57
 */
@ThreadSafe
public class ImmutableDemo {
    public static final Integer a = 1;
    public static final String b = "2";
    private static Map<Integer, String> map = Maps.newHashMap();
    public static final ImmutableList list = ImmutableList.of(1, 2, 3);
    public static final ImmutableMap immutableMap = ImmutableMap.<String,String>builder()
            .put("1","1")
            .put("1","2")
            .build();

    static {
        map.put(1, "1");
        map.put(2, "2");
        map.put(3, "3");
        map = Collections.unmodifiableMap(map);
    }

    public static void main(String[] args) {
        // 会抛异常
//        map.put(1,"3");
//        list.add(4);
    }
}
