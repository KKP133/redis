package com.tea.modules.java8.innerclass;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * com.tea.modules.java8.innerclass <br>
 * 内部类的理解
 *
 * @author jaymin
 * @since 2021/9/1
 */
interface Print {
    void print(String message);
}

public class Out {
    private Integer value;

    @AllArgsConstructor
    @NoArgsConstructor
    class Inner {
        private int i = 0;

        public int value() {
            System.out.println(Out.this.value);
            return i;
        }
    }

    public Print printer() {
        final class MyPrinter implements Print {

            @Override
            public void print(String message) {
                System.out.println("我是隐藏在方法中的类:" + message);
            }
        }
        return new MyPrinter();
    }

    public Inner setValue(int i) {
        return new Inner(i);
    }

    public void printInnerInfo() {
        Inner inner = new Inner();
        System.out.println(inner.value());
    }

    public static void main(String[] args) {
        Out out = new Out();
        out.printer().print("局部内部类");
    }

    private static void newInner() {
        Out out = new Out();
        Inner inner = out.new Inner();
        System.out.println(inner.value());
    }

    private static void example() {
        Out out = new Out();
        Inner inner = out.setValue(2);
        System.out.println(inner.value());
    }
}
