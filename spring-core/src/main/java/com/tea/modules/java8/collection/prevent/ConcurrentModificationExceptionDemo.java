package com.tea.modules.java8.collection.prevent;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * com.tea.modules.java8.collection.prevent <br>
 * 触发集合的ConcurrentModificationException <br>
 * 可以想象， 如果在某个迭代器修改集合时， 另一个迭代器对其进行遍历，一定会出现
 * 混乱的状况。例如，一个迭代器指向另一个迭代器刚刚删除的元素前面，现在这个迭代器
 * 就是无效的，并且不应该再使用。链表迭代器的设计使它能够检测到这种修改。如果迭代
 * 器发现它的集合被另一个迭代器修改了， 或是被该集合自身的方法修改了， 就会抛出一个
 * ConcurrentModificationException 异常 <br>
 * JDK文档:   <br>
 * 例如，通常不允许一个线程修改一个集合，而另一个线程正在对其进行迭代。  <br>
 * 通常，在这些情况下迭代的结果是不确定的。   <br>
 * 如果检测到此行为，某些迭代器实现（包括 JRE 提供的所有通用集合实现的实现）可能会选择抛出此异常。   <br>
 * 这样做的迭代器被称为快速失败迭代器，因为它们快速而干净地失败，  <br>
 * 而不是冒着在未来不确定的时间出现任意、非确定性行为的风险。<br>
 * @author jaymin
 * @since 2021/9/18
 */
public class ConcurrentModificationExceptionDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        ListIterator<String> stringListIteratorA = list.listIterator();
        ListIterator<String> stringListIteratorB = list.listIterator();
        stringListIteratorA.next();
        stringListIteratorA.remove();
        stringListIteratorB.next();
    }
}
