package com.tea.modules.java8.thread.publish;

import com.tea.modules.java8.thread.annotation.ThreadUnSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

/**
 * 不安全的发布对象
 *
 * @author jaymin
 * @since 2022/3/3 23:37
 */
@ThreadUnSafe
@Slf4j
public class UnSafePublish {
    private String[] states = {"a", "b", "c"};

    public String[] getStates() {
        return states;
    }

    public static void main(String[] args) {
        UnSafePublish unSafePublish = new UnSafePublish();
        log.info("{}", Arrays.toString(unSafePublish.getStates()));
        // 通过公有方法提供了同一个私域states,外部线程可以随意篡改
        String[] states = unSafePublish.getStates();
        states[0] = "aa";
        log.info("{}", Arrays.toString(unSafePublish.getStates()));

    }
}
