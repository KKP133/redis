package com.tea.modules.algorithm;

/**
 * com.tea.modules.algorithm
 *
 * @author jaymin
 * @since 2022/1/11
 */
public class 递归 {
    private static int index = 5;

    public static void main(String[] args) {
        doSync(0);
    }

    private static void doSync(int i) {
        if (index <= 0) {
            return;
        }
        System.out.println("我在遍历第" + i + "次");
        i = i + 500;
        index = index -1;
        doSync(i);
    }
}
