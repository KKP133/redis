package com.tea.modules.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tea.modules.model.po.Device;

/**
 * com.tea.modules.json
 *
 * @author jaymin
 * @since 2022/1/10
 */
public class JacksonDemo {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Device device = new Device();
        device.setProperty("111");
        device.setType("y");
        String json = objectMapper.writeValueAsString(device);
        System.err.println(json);
    }
}
