package com.tea.modules.easyexcel.convert;

import com.spire.xls.FileFormat;
import com.spire.xls.Workbook;
import com.spire.xls.Worksheet;

/**
 * com.tea.modules.easyexcel.convert
 *
 * @author jaymin
 * @since 2022/5/19
 */
public class SpireDemo {

    public static void main(String[] args) {
        //加载Excel工作表
        Workbook wb = new Workbook();
        // 绝对路径好像是
        wb.loadFromFile(".xls");

        //获取工作表
        Worksheet sheet = wb.getWorksheets().get(0);

        //调用方法将Excel工作表保存为图片
        sheet.saveToImage("ToImg.png");
        //调用方法，将指定Excel单元格数据范围保存为图片
        //sheet.saveToImage("ToImg2.png",8,1,30,7);

        //调用方法将Excel保存为HTML
        sheet.saveToHtml("ToHtml.html");
    }
}
