package com.tea.modules.guava;

import com.google.common.base.Preconditions;
import com.google.zxing.common.StringUtils;

/**
 * com.tea.modules.guava <br>
 * 让方法调用的前置条件判断更简单
 *
 * @author jaymin
 * @since 2023/2/9
 */
public class PreconditionsOfGuava {
    public static void main(String[] args) {
        // 判断表达式内的条件是否成立，默认抛出
        Preconditions.checkArgument(1 > 2);
        // 能表达异常信息的检查
        Preconditions.checkArgument(1 > 2, "1怎么可能大于2");
        Preconditions.checkNotNull(null,"空的，你不知道么");
    }
}
