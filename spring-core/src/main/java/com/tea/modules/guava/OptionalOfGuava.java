package com.tea.modules.guava;

import com.google.common.base.Optional;

import java.util.Set;

/**
 * com.tea.modules.guava
 * 避免出现空指针异常 <br>
 * guava在很早的版本就应用了Optional，JDK8后引入到公共的api了
 * @author jaymin
 * @since 2023/2/9
 */
public class OptionalOfGuava {

    public static void main(String[] args) {
        boolean isNull = Optional.fromNullable(null).isPresent();
        System.out.println("我是不是非空的:" + isNull);
        Object orNull = Optional.fromNullable(null).orNull();
        System.out.println("如果是空的，那我返回:" + orNull);
        // 等价于Java的Optional.empty()
        Object empty = Optional.absent();
        Set<String> asSet = Optional.fromNullable("我不是空的").asSet();
        System.out.println(asSet);
    }
}
