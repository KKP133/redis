package com.tea.modules.retry;

import com.tea.modules.exception.RestfulException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

/**
 * com.tea.modules.retry
 *
 * @author jaymin
 * @since 2023/2/9
 */
@Service
@Slf4j
public class RetryRequestService {

    /**
     * value = RuntimeException.class：是指方法抛出RuntimeException异常时，进行重试。这里可以指定你想要拦截的异常。 <br>
     * maxAttempts：是最大重试次数。如果不写，则是默认3次。 <br>
     * backoff = @Backoff(delay = 100)：是指重试间隔。delay=100意味着下一次的重试，要等100毫秒之后才能执行。 <br>
     * @param param 参数
     * @return
     */
    @Retryable(value = RuntimeException.class, maxAttempts = 5, backoff = @Backoff(delay = 100))
    public void request(String param){
        log.error("我艹了，又报错");
        throw new RestfulException("别报错了，球球你");
    }

    @Recover
    public void recover(RuntimeException runtimeException){
        log.error("你真牛逼，报错5次");
    }
}
