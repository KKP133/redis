package com.tea.modules.retry;

import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

/**
 * com.tea.modules.retry
 *
 * @author jaymin
 * @since 2023/2/9
 */
@Configuration
@EnableRetry
public class SpringRetryDemo {
}
