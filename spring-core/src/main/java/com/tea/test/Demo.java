package com.tea.test;

import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tea.modules.model.po.Device;
import com.tea.modules.model.po.Student;
import com.tea.modules.util.LocalDateUtil;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * com.tea.test
 *
 * @author jaymin
 * @since 2021/7/8
 */
public class Demo {
    public static void main(String[] args) {
        String a = "plugin-private://wx34345ae5855f892d/pages/productDetail/productDetail?productId=104205908";
        String pages = a.substring(a.indexOf("pages"));
        System.out.println(pages);
    }
}
