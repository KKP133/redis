package com.tea.test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jaymin
 * @since 2021/6/24 23:25
 */
public class IDEADemo {
    public static void main(String[] args) {
        printYesterday();
        printYesterdayEndTime();
    }

    /**
     * 打印昨天的日期
     */
    private static void printYesterday() {
        LocalDate yesterday = getYesterday();
        System.out.println(yesterday);
    }

    /**
     * 获取昨天的时间
     * @return
     */
    private static LocalDate getYesterday() {
        LocalDate now = LocalDate.now();
        return now.plusDays(-1);
    }

    /**
     * 打印昨天最后的日期
     */
    private static void printYesterdayEndTime(){
        LocalDate yesterday = getYesterday();
        LocalDateTime yesterdayEndTime = yesterday.atTime(LocalTime.MAX);
        System.out.println(yesterdayEndTime);
    }

}
