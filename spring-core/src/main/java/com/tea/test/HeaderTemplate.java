package com.tea.test;

import com.google.common.collect.Lists;
import com.tea.modules.model.po.Student;
import com.tea.modules.model.po.User;
import org.springframework.beans.BeanUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * com.tea.test
 *
 * @author jaymin
 * @since 2021/7/8
 */
public class HeaderTemplate {

    public static void main(String[] args) throws UnsupportedEncodingException {
/*        User user = new User();
        Student student = new Student();
        user.setAge(1);
        BeanUtils.copyProperties(user,student);
        System.out.println(student.toString());*/
/*        List list = new ArrayList();
        List partition = Lists.partition(list, 500);
        System.out.println(partition.size());*/
        String s1 = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImNvcnBvcmF0aW9uSWQiOjEsImNyZWF0ZWQiOjE2MzY5ODEzOTQ5MjgsIm1hbGxJZCI6MTAwMDAsImNvcnBvcmF0aW9uTmFtZSI6IuW5v-W3nuS4u-S9kyIsIm5pY2tOYW1lIjoi6LaF57qn566h55CG5ZGYIiwid3hDb3JwSWQiOiJ3dzJjNzlmMzU4M2M4MjMyMTkiLCJleHAiOjE2MzcwNjc3OTR9.mT4lvfrtlWt5HufwF3Lkjj2qBcsQyhUuhBFPq8nBLslJIhdGnDE0yBC64uV3uRoCP8-nxKgA1EY4ICUmihtrzA";
        System.out.println("编码前的:" + s1);
        String encode = URLEncoder.encode(s1, "utf-8");
        System.out.println("编码后的:" + encode);
        String d = encode.replace("+", "%20");
        System.out.println("空格化:" + d);
        System.out.println("解码后:" + URLDecoder.decode(d, "utf-8"));
        System.out.println(s1.length());
        String s2 = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJlcnAiLCJjb3Jwb3JhdGlvbklkIjoxLCJjcmVhdGVkIjoxNjM2OTc5MjYwNjUwLCJtYWxsSWQiOm51bGwsImNvcnBvcmF0aW9uTmFtZSI6IuW5v-W3nuS4u-S9kyIsIm5pY2tOYW1lIjoi5oiR55qE56WeIiwid3hDb3JwSWQiOiJ3dzJjNzlmMzU4M2M4MjMyMTkiLCJleHAiOjE2MzcwNjU2NjB9.XDnkLouv7r8C0jGSuE2i7m3ESDVA_45YiB1aw8XbZB-0Oy_J66O0SB1XleInDfQEVK9af-HPk_eb_9ZGkmD-lg";
        System.out.println(s2.length());
        System.out.println("FK2021111100000002_MALL_10000".length());
    }
}
