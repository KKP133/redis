package com.tea.test;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.util.StopWatch;

import java.util.Arrays;
import java.util.Date;

/**
 * com.tea.test <br>
 * 获取一个方法执行的时间
 * @author jaymin
 * @since 2022/1/25
 */
public class MethodExecuteTimeTest {

    public static void main(String[] args) throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("first method");
        Thread.sleep(1000);
        stopWatch.stop();
        stopWatch.start("second method");
        Thread.sleep(2000);
        stopWatch.stop();
        StopWatch.TaskInfo[] taskInfos = stopWatch.getTaskInfo();
        Arrays.stream(taskInfos)
                .forEach(s-> System.out.println(JSON.toJSONString(s)));

        String yyyyMMdd = DateUtil.format(new Date(), "yyyyMMdd");
        System.out.println(yyyyMMdd);
    }
}
