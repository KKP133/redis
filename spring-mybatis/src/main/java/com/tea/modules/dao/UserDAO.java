package com.tea.modules.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tea.modules.pojo.UserDO;

/**
 * com.tea.modules.dao
 *
 * @author jaymin
 * @since 2022/9/29
 */
public interface UserDAO extends BaseMapper<UserDO> {
}
