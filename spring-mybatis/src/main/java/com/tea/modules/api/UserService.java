package com.tea.modules.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tea.modules.pojo.UserDO;

/**
 * com.tea.modules.api
 *
 * @author jaymin
 * @since 2022/9/29
 */
public interface UserService extends IService<UserDO> {
    void saveInT();
}
