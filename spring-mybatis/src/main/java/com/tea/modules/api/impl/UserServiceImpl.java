package com.tea.modules.api.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tea.modules.api.UserService;
import com.tea.modules.dao.UserDAO;
import com.tea.modules.pojo.UserDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * com.tea.modules.api.impl
 *
 * @author jaymin
 * @since 2022/9/29
 */
@Service("myUserService")
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserDAO, UserDO> implements UserService {


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveInT() {
        try {
            save(UserDO.builder().userName("A").password("1").build());
        } catch (Exception e) {
            log.error("我抓到你了！！！");
        }
        save(UserDO.builder().userName("B").password("1").build());
    }
}
