package com.tea.modules.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * com.tea.modules.pojo
 *
 * @author jaymin
 * @since 2022/9/29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("user")
public class UserDO implements Serializable {
    @TableId(type = IdType.AUTO)
    private Long id;
    private String userName;
    private String password;
}
