package com.tea.modules.bean.di.autowireds;

import com.tea.modules.bean.di.UserService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * com.tea.modules.bean.di
 * <a href="https://www.cnblogs.com/yourbatman/p/13328817.html">动态注入</a>
 * @author jaymin
 * @since 2022/1/12
 */
public class DemoUtils {

    private static UserService vipUserService;

    @Autowired
    public void setting(UserService vipUserService) {
        DemoUtils.vipUserService = vipUserService;
    }

    public static void doLogin() {
        vipUserService.login();
    }
}
