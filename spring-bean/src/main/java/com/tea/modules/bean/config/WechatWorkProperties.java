package com.tea.modules.bean.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author jaymin
 * @since 2021/10/9
 */
@Configuration
@Slf4j
@Setter
@Getter
@ConfigurationProperties(prefix = "wechat.work")
public class WechatWorkProperties {
    /**
     * 企业号ID
     */
    private String corpId;
    /**
     * 明远App应用配置
     */
    private AppConfig mingYuanAppConfig;
    /**
     * 通讯录配置
     */
    private AppConfig addressBookConfig;

    @Getter
    @Setter
    public static class AppConfig {
        private String suiteId;
        /**
         * 设置企业微信应用的基础URL
         */
        private String baseUrl = "https://qyapi.weixin.qq.com";
        /**
         * 设置企业微信应用的tenantId
         */
        private String tenantId;
        /**
         * 设置企业微信应用的AgentId
         */
        private Integer agentId;

        /**
         * 设置企业微信应用的Secret
         */
        private String secret;

        /**
         * 设置企业微信应用的token
         */
        private String token;

        /**
         * 设置企业微信应用的EncodingAESKey
         */
        private String aesKey;

    }
}
