package com.tea.modules.bean.order;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

/**
 * @author jaymin
 * @create 2023-04-18 15:50
 */
@Order(1)
@Service
public class BOrderImpl implements IOrder{
    @Override
    public void doSomething() {
        System.out.println("我是B啊");
    }
}
