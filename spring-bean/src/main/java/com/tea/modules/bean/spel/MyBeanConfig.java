package com.tea.modules.bean.spel;

import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * com.tea.modules.bean.spel
 *
 * @author jaymin
 * @since 2022/1/6
 */
@Configuration
@Getter
public class MyBeanConfig {

    @Value("#{'${com.tea.customer.names}'.split(',')}")
    private List<String> customerNames;

    @Value("https://${tea.server.host}")
    private String url;
}
