package com.tea.modules.bean.di.autowireds;

import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * com.tea.modules.bean.di.autowireds
 *
 * @author jaymin
 * @since 2022/1/12
 */
@Component
public class AutowiredFactory implements SmartInitializingSingleton {

    @Autowired
    private AutowireCapableBeanFactory beanFactory;

    @Override
    public void afterSingletonsInstantiated() {
        beanFactory.autowireBean(new DemoUtils());
    }
}
