package com.tea.modules.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

/**
 * @author jaymin
 * @since 2022/1/3 22:21
 */
@Slf4j
public class MyPartition implements Partitioner {
    /**
     * 发送的消息为jay-i这种形式
     * @param topic
     * @param key
     * @param keyBytes
     * @param value
     * @param valueBytes
     * @param cluster
     * @return
     */
    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        String keyStr = key + "";
        String index = keyStr.substring(4);
        log.info("key:{},index:{}", keyStr, index);
        int resultIndex = Integer.parseInt(index) % 2;
        return 0;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}
