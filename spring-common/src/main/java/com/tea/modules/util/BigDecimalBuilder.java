package com.tea.modules.util;

import java.math.BigDecimal;

/**
 * com.tea.modules.util
 *
 * @author jaymin
 * @since 2022/7/1
 */
public class BigDecimalBuilder {

    private BigDecimal val;

    public static void main(String[] args) {
        BigDecimal a = new BigDecimal("1");
        BigDecimal b = new BigDecimal("1");
        BigDecimal c = new BigDecimal("1000");
        BigDecimal d = new BigDecimal("1");
        BigDecimal value = BigDecimalHelper
                .build("0")
                .and(bigDecimalHelper -> bigDecimalHelper.init(a).div("0.5").sub(h -> h.init(b).mul(c).div("1000")))
                .mul(d)
                .getValue();
        System.out.println(value);
        BigDecimal divide = a.divide(new BigDecimal("0.5"), 4, BigDecimal.ROUND_HALF_UP);
        BigDecimal divide1 = b.multiply(c).divide(new BigDecimal("1000"), 4, BigDecimal.ROUND_HALF_UP);
        BigDecimal result = BigDecimalHelper.build(divide)
                .sub(divide1)
                .mul(d)
                .getValue();
        System.out.println(result);
    }

}
