/*
package com.tea.modules.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jk.share.core.utils.HttpUtils;
import com.tea.modules.exception.RestfulException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.ResourceAccessException;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

*/
/**
 * @Author yuanhan
 * @Date 2019-11-18 20:46
 * @Description
 *//*

@Slf4j
public class OpenApiHttpUtils {

    private static OkHttpClient client;

    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/*");

    public static JSONObject retryPostV2(String url, Object object, Map<String, String> headers) {
        int retry = 2;
        while (retry > 0) {
            String s = null;
            JSONObject rsp = null;
            try {
                s = post(url, object, headers);
                rsp = parseJSONObject(s);
                if (rsp == null || !rsp.getBoolean("success")) {
                    retry--;
                    if (retry > 0) {
                        continue;
                    } else {
                        return rsp;
                    }
                }

            } catch (Exception e) {
                log.error("url error:" + url + ":" + s);
                log.error("请求失败", e);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    log.error("interrupt", ex);
                }
                retry--;
                continue;
            }
            return rsp;
        }
        return null;
    }

    public static String post(String url, Object body, Map<String, String> headers) {
        try {
            log.info("调用第三方http post请求 {}  参数：---body---{}---headers---{}",
                    url,
                    JSONObject.toJSONString(body, SerializerFeature.WriteMapNullValue),
                    JSONObject.toJSONString(headers, SerializerFeature.WriteMapNullValue));
            String response = HttpUtils.postObjectWithHeaderByClient(url, body, client, headers);
            log.info("调用第三方http post请求 {}  返回体：{}", url, JSONObject.toJSONString(response));
            return response;
        } catch (IOException e) {
            log.error("调第三方http接口 {}  异常，error:{}", url, e.getMessage());
            throw new RestfulException("外部接口异常");
        }
    }

    public static String get(String url, Map<String, String> params) {
        try {
            log.info("调用第三方http get请求 {}  参数：\n---params---\n{}\n---headers---\n{}",
                    url,
                    JSONObject.toJSONString(params, SerializerFeature.WriteMapNullValue));
            String response = HttpUtils.getWithClient(url, params, client);
            log.info("调用第三方http get请求 {}  返回体：\n{}", url, JSONObject.toJSONString(response));
            return response;
        } catch (IOException e) {
            log.error("调第三方http接口 {}  异常，error:{}", url, e.getMessage());
            throw new RestfulException("外部接口异常");
        }
    }

    public static String get(String url, Map<String, String> params, Map<String, String> headers) {
        try {
            log.info("调用第三方http get请求 {}  参数：\n---params---\n{}\n---headers---\n{}",
                    url,
                    JSONObject.toJSONString(params, SerializerFeature.WriteMapNullValue),
                    JSONObject.toJSONString(headers, SerializerFeature.WriteMapNullValue));
            String response = HttpUtils.getWithClient(url, params, headers, client);
            log.info("调用第三方http get请求 {}  返回体：\n{}", url, JSONObject.toJSONString(response));
            return response;
        } catch (IOException e) {
            log.error("调第三方http接口 {}  异常，error:{}", url, e.getMessage());
            throw new RestfulException("外部接口异常");
        }
    }

    public static String postForm(String url, Map<String, String> params, Map<String, String> headers) {
        try {
            log.info("调用第三方http post form请求 {}  参数：\n---params---\n{}\n---headers---\n{}",
                    url,
                    JSONObject.toJSONString(params, SerializerFeature.WriteMapNullValue),
                    JSONObject.toJSONString(headers, SerializerFeature.WriteMapNullValue));
            String response = HttpUtils.postFormWithHeader(url, params, headers);
            log.info("调用第三方http post form请求 {}  返回体：\n{}", url, JSONObject.toJSONString(response));
            return response;
        } catch (IOException e) {
            log.error("调第三方http接口 {}  异常，error:{}", url, e.getMessage());
            throw new RestfulException("外部接口异常");
        }
    }

    public static String postFormWithFile(String url, String fileKey, String fileName, String filePath, Map<String, String> params, Map<String, String> headers) {
        try {
            log.info("调用第三方http post form with file请求 {}  参数：\n---params---\n{}\n---headers---\n{}\n---fileKey---\n{}\n---fileName---\n{}\n---filePath---\n{}",
                    url,
                    JSONObject.toJSONString(params, SerializerFeature.WriteMapNullValue),
                    JSONObject.toJSONString(headers, SerializerFeature.WriteMapNullValue),
                    fileKey,
                    fileName,
                    filePath);
            //创建RequestBody
            RequestBody fileBody = RequestBody.create(MEDIA_TYPE_PNG, new File(filePath));

            //构建MultipartBody
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(fileKey, fileName, fileBody)
                    .build();
            //放入header
            Request.Builder requestBuilder = new Request.Builder();
            fixHeader(headers, requestBuilder);

            //构建请求
            Request request = requestBuilder
                    .url(url)
                    .post(requestBody)
                    .build();

            //发送请求
            Response response = client.newCall(request).execute();
            assert response.body() != null;
            String result = response.body().string();
            log.info("调用第三方http post form with file请求 {}  返回体：\n{}", url, JSONObject.toJSON(result));
            return result;
        } catch (Exception e) {
            log.error("调第三方http接口 {}  异常，error:{}", url, e.getMessage());
            throw new RestfulException("外部接口异常");
        }
    }

    public static String post(String url, Object body) {
        return post(url, body, null);
    }

    */
/**
     * 发送鲁厂post请求
     *
     * @param url  url
     * @param body 请求体
     * @return
     *//*

    public static JSONObject postLuWithHeaderAndUrl(String url, Object body, Map<String, String> headers) {
        String result = post(url, body, headers);
        return parseJSONObject(result);
    }


    */
/**
     * String转JSONObject
     *
     * @param response
     * @return
     *//*

    public static JSONObject responseToJSONObject(String response) {
        try {
            return JSONObject.parseObject(response);
        } catch (Exception e) {
            log.error("string转jsonObject异常：{}", e.getMessage());
            throw new RestfulException("外部接口数据处理异常，请联系管理员");
        }
    }

    public static JSONObject parseJSONObject(String response) {
        try {
            if (response.startsWith("[")) {
                return JSONArray.parseObject(response);
            } else {
                return JSONObject.parseObject(response);
            }
        } catch (Exception e) {
            log.error("外部接口数据解析异常", e);
            throw new RestfulException("外部接口数据解析异常");
        }
    }

    public static <T> T parseJSONObject(String response, TypeReference<T> typeReference) {
        if (response.startsWith("[")) {
            return JSONArray.parseObject(response, typeReference);
        } else {
            return JSONObject.parseObject(response, typeReference);
        }
    }

    */
/**
     * 返回的ResultDTO是否成功
     *
     * @param response
     * @return
     *//*

    public static Boolean isResultDTOSuccess(JSONObject response) {
        return response.getBoolean("success");
    }


    private static void fixHeader(Map<String, String> headers, Request.Builder requestBuilder) {
        if (headers != null && headers.size() > 0) {
            Iterator var2 = headers.entrySet().iterator();

            while (var2.hasNext()) {
                Map.Entry<String, String> h = (Map.Entry) var2.next();
                String headerKey = (String) h.getKey();
                String headerVal = (String) h.getValue();
                if (!StringUtils.isBlank(headerKey) && !StringUtils.isBlank(headerVal)) {
                    requestBuilder.addHeader(headerKey, headerVal);
                }
            }
        }

    }

    static {
        client = (new OkHttpClient.Builder())
                .connectTimeout(3L, TimeUnit.SECONDS)
                .writeTimeout(60L, TimeUnit.SECONDS)
                .readTimeout(60L, TimeUnit.SECONDS)
                .build();
    }
}
*/
