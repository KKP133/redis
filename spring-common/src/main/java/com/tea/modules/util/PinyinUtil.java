package com.tea.modules.util;

import cn.hutool.extra.pinyin.PinyinEngine;
import org.apache.commons.lang3.StringUtils;

/**
 * com.tea.modules.util
 *
 * @author jaymin
 * @since 2022/2/22
 */
public class PinyinUtil {
    public static void main(String[] args) {
        PinyinEngine engine = new SpecialPinyin4jEngineOfFixBug();
        String pinyin = engine.getPinyin("长沙张剑波0224-1", " ");
        System.out.println(StringUtils.replace(pinyin," ",""));
    }
}
