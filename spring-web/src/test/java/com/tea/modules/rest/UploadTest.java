package com.tea.modules.rest;

import com.alibaba.fastjson.JSONObject;
import com.tea.modules.model.response.MediaUploadResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * com.tea.modules.rest
 *
 * @author jaymin
 * @since 2021/11/17
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UploadTest {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void test() throws FileNotFoundException {
        String name = "C:\\Users\\jaymin\\Desktop\\Jaymin\\test.png";
        File file = new File(name);
        MultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
        // 将字节数组转成 ByteArrayResource
        request.add("file", new FileSystemResource(file));
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(request, getHeaders("test.png",file.length()));
        String url = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token=ps7PSPGxVHl7YmUSSiLwvlyy9eSZNS72kKaPAZXzOVu-5eVNCtnmu3CCEeTmhQNLRaDyNlttuC1L16vcZL-U7zEmO4fcAVUy2lSlB_jgOoElghmLFiu_7i6yFma6OPZ2Lhed64kvrZdkyXU1P3jaT8xxjYyjo7hSC71q552JKJHxVhhgd_KDk-NDH1evtojD_b1-SjVs7_k-maIvjQ18gw&type=image";
        JSONObject resultJson = restTemplate.postForObject(url, httpEntity, JSONObject.class);
        MediaUploadResponse mediaUploadResponse = resultJson.toJavaObject(MediaUploadResponse.class);
        System.out.println(mediaUploadResponse.toString());
    }


    /**
     * 获取 form-data 的请求头对象
     *
     * @return
     */
    public static HttpHeaders getHeaders(String fileName, Long fileLength) {
        // 设置请求头
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE,"application/octet-stream");
        // form-data方式提交
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        String applicationOctetStreamValue = MediaType.APPLICATION_OCTET_STREAM_VALUE;
        headers.set(HttpHeaders.CONTENT_TYPE, applicationOctetStreamValue);
        ContentDisposition contentDisposition = ContentDisposition.builder("form-data")
                .filename(fileName)
                .name("media")
                .size(fileLength)
                .build();
        headers.setContentDisposition(contentDisposition);
        return headers;
    }
}
