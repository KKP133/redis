package com.tea.modules.es;

import cn.easyes.core.conditions.LambdaEsQueryWrapper;
import com.tea.modules.data.es.entity.EsDTO;
import com.tea.modules.data.es.maaper.MyMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

/**
 * com.tea.modules.es
 *
 * @author jaymin
 * @since 2022/8/25
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class EsTest {
    @Autowired
    private MyMapper myMapper;

    @Test
    public void testGroupBy() throws IOException {
        LambdaEsQueryWrapper<EsDTO> wrapper = new LambdaEsQueryWrapper<>();
        wrapper.index("wp_igkhgaacqihmxxyvso7pkaeplfivg_marketing_automation_client");
        wrapper.limit(10);
        List<EsDTO> esDTOList = myMapper.selectList(wrapper);
        System.out.println(esDTOList);
    }
}
