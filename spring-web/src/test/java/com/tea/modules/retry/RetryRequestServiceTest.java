package com.tea.modules.retry;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * com.tea.modules.retry
 *
 * @author jaymin
 * @since 2023/2/9
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RetryRequestServiceTest {
    @Autowired
    private RetryRequestService retryRequestService;

    @Test
    public void test(){
        retryRequestService.request("111");
    }
}
