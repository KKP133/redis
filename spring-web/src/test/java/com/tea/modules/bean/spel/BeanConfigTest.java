package com.tea.modules.bean.spel;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * com.tea.modules.bean.spel
 *
 * @author jaymin
 * @since 2022/1/6
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BeanConfigTest {
    @Autowired
    private MyBeanConfig beanConfig;

    @Test
    public void test() {
        beanConfig.getCustomerNames().forEach(System.out::println);
        System.out.println(beanConfig.getUrl());
    }
}
