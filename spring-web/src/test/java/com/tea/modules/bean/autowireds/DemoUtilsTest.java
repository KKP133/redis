package com.tea.modules.bean.autowireds;

import com.tea.modules.bean.di.autowireds.DemoUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * com.tea.modules.bean.autowireds
 *
 * @author jaymin
 * @since 2022/1/12
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DemoUtilsTest {
    @Test
    public void test() throws InterruptedException {
        DemoUtils.doLogin();
    }
}
