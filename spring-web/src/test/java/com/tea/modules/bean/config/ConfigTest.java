package com.tea.modules.bean.config;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * com.tea.modules.bean.config
 *
 * @author jaymin
 * @since 2021/10/19
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ConfigTest {
    @Autowired
    private WechatWorkProperties wechatWorkProperties;

    @Test
    public void test(){
        System.err.println(wechatWorkProperties.getMingYuanAppConfig().getSuiteId());
    }
}
