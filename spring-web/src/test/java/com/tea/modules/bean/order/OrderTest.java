package com.tea.modules.bean.order;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author jaymin
 * @create 2023-04-18 15:52
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderTest {
    @Autowired
    private List<IOrder> orderList;
    @Test
    public void test(){
        orderList.forEach(IOrder::doSomething);
    }
}
