package com.tea.modules.core;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * com.tea.modules.core
 *
 * @author jaymin
 * @since 2021/10/11
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "com.tea.path")
public class ConfigTest {
    private String monitor;

}
