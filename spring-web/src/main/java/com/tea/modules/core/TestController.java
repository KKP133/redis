package com.tea.modules.core;

import com.alibaba.fastjson.JSON;
import com.tea.modules.model.po.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * com.tea.modules.core
 *
 * @author jaymin
 * @since 2021/7/29
 */
@RestController
@RequestMapping("/hello")
@Slf4j
public class TestController {

    @GetMapping("")
    public void hello(Student student) {
        log.info("有人请求我:{}", JSON.toJSONString(student));
        return;
    }
}
