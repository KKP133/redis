package com.tea.modules.core;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * com.tea.modules.core
 *
 * @author jaymin
 * @since 2021/10/20
 */
@RestController
@RequestMapping("/redirect")
public class RedirectURLController {


    @GetMapping(value = "")
    public void redirectByKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String data = "passed-data";
        String url = "http://localhost:8080/dosomething";
        Cookie cookie = new Cookie("corpId", "123456");
        cookie.setPath("/");
        response.addCookie(cookie);
        response.sendRedirect(url);
    }
}
