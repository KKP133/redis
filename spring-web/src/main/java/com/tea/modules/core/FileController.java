package com.tea.modules.core;


import cn.hutool.log.Log;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.tea.modules.exception.RestfulException;
import com.tea.modules.model.po.Student;
import com.tea.modules.model.po.UploadFileVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * com.jay.company.file
 *
 * @author xiejiemin
 * @create 2020/9/27
 */
@RestController("coreFileController")
@RequestMapping("/file")
@Slf4j
public class FileController {
    @Resource
    private RestTemplate restTemplate;

    @PostMapping("/upload")
    public void uploadFile(@ModelAttribute UploadFileVO uploadFileVO) {
        uploadFileVO.getFiles().stream().forEach(f -> System.out.println(f.getOriginalFilename()));
        String sign = uploadFileVO.getSign();
        verify();
        uploadFileVO.getFiles().forEach(multipartFile -> {
            try (InputStream inputStream = multipartFile.getInputStream();
                 OutputStream outStream = new FileOutputStream("C:\\Users\\jaymin\\Desktop\\fsdownload\\" + multipartFile.getOriginalFilename())) {
                byte[] buffer = new byte[8 * 1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                log.error("文件转存出现异常:" + e);
            }
        });
    }

    @PostMapping("/send")
    public void sendFile() {
        MultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
        // 设置请求头
        HttpHeaders headers = new HttpHeaders();
        // form-data方式提交
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        List<String> multipartFiles = Lists.newArrayList("C:\\Users\\jaymin\\Desktop\\monitor\\ZH.txt",
                "C:\\Users\\jaymin\\Desktop\\monitor\\ZH解密.txt");
        multipartFiles.forEach(multipartFile -> {

            byte[] bytes = multipartFile.getBytes();
            // 将字节数组转成 ByteArrayResource
            ByteArrayResource byteArrayResource = new ByteArrayResource(bytes) {
                @Override
                public String getFilename() {
                    return multipartFile.substring(multipartFile.lastIndexOf(File.separator) + 1);
                }
            };
            request.add("files", byteArrayResource);
        });
        // 用HttpEntity封装请求报文
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(request, headers);
        restTemplate.postForObject("http://localhost:8080/file/upload", httpEntity, JSONObject.class);
    }

    private void verify() {

    }

    @GetMapping("/download")
    public void downloadFileFromCos() throws IOException {
        String url = "";
        URL httpsUrl = new URL(url);
        String fileName = "测试.png";
        File file = new File("C:/Users/jaymin/Desktop/file/" + fileName);
        log.info("文件名:{}", file.getName());
        log.info("开始下载");
        FileUtils.copyURLToFile(httpsUrl, file);
        MultipartFile forObject = restTemplate.getForObject(url, MultipartFile.class);
        log.info(forObject.toString());
    }

    @GetMapping("/tranUrlToFile")
    public MultipartFile getFile(@RequestParam("url") String url, @RequestParam("fileName") String fileName) {
        FileItem item = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setReadTimeout(30000);
            conn.setConnectTimeout(30000);
            //设置应用程序要从网络连接读取数据
            conn.setDoInput(true);
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                try (InputStream is = conn.getInputStream()) {
                    try (OutputStream os = item.getOutputStream()) {
                        FileItemFactory factory = new DiskFileItemFactory(1024 * 100, null);
                        item = factory.createItem(fileName, "application/octet-stream", false, fileName);
                        int bytesRead = 0;
                        byte[] buffer = new byte[1024 * 100];
                        while ((bytesRead = is.read(buffer, 0, buffer.length)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("文件下载失败", e);
        }
        MultipartFile multipartFile = new CommonsMultipartFile(item);
        return multipartFile;
    }

    @GetMapping("/download/excel")
    public void downExcel(HttpServletResponse response){
        try {
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(2022-11 + "电厂预结算单.xlsx", "UTF-8"));
            ArrayList<Student> students = Lists.newArrayList(
                    Student.builder().age(1).name("A").build(),
                    Student.builder().age(1).name("A").build(),
                    Student.builder().age(2).name("B").build(),
                    Student.builder().age(2).name("B").build(),
                    Student.builder().age(3).name("B").build());
            // 供电信息写入
            EasyExcelFactory.write(response.getOutputStream()).sheet("结算单").doWrite(students);
        } catch (Exception e) {
            Log.get().error(e, "导出失败！");
        }
    }
}
