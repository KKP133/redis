package com.tea.modules.common.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jaymin
 * @since 2022/3/5 23:14
 */
@RequestMapping("/threadLocal")
@RestController
public class FilterController {

    @GetMapping("")
    public void hello() {
        return;
    }
}
