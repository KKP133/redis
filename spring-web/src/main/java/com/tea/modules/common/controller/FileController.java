package com.tea.modules.common.controller;

import com.tea.modules.util.FileUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author jaymin
 * 2021/2/27 20:34
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @PostMapping("type")
    public String getFileType(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        FileUtils.copyInputStreamToFile(multipartFile.getInputStream(),new File("C:\\Users\\jaymin\\Desktop\\"+multipartFile.getOriginalFilename()));
        return String.format("current file type is : [%s]", FileUtil.getFileTypeByStream(multipartFile.getBytes()));
    }
}
