package com.tea;

import cn.easyes.starter.register.EsMapperScan;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author jaymin
 */
@SpringBootApplication(scanBasePackages = {"com.tea"})
@Slf4j
@EnableAspectJAutoProxy
@EnableScheduling
@EsMapperScan("com.tea.modules.data.es.maaper")
@MapperScan("com.tea.modules.dao")
public class CodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeApplication.class, args);
    }
}
