package com.tea.modules.model.response;

import lombok.Data;

import java.io.Serializable;

/**
 * com.partschance.mall.wechatwork.response
 *
 * @author jaymin
 * @since 2021/11/16
 */
@Data
public class MediaUploadResponse extends BaseResponse implements Serializable {
    /**
     * 媒体文件类型，分别有图片（image）、语音（voice）、视频（video），普通文件(file)
     */
    private String type;
    /**
     * 媒体文件上传后获取的唯一标识，3天内有效
     */
    private String mediaId;
    /**
     * 媒体文件上传时间戳
     */
    private String createdAt;
}
