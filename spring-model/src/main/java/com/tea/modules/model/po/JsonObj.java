package com.tea.modules.model.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * com.tea.modules.model.po
 *
 * @author jaymin
 * @since 2022/4/12
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JsonObj implements Serializable {
    private Integer id;
    private JsonNo jsonNo;

    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class JsonNo implements Serializable {
        private Integer id;
        private BsonNo bsonNo;
    }

    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class BsonNo implements Serializable {
        private Integer bId;
    }
}
