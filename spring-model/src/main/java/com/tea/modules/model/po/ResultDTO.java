package com.tea.modules.model.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * com.tea.modules.model.po
 *
 * @author jaymin
 * @since 2022/9/16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResultDTO {
    private String key;
    private String roleId;
}
