package com.tea.modules.model.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * com.baturu.wechatwork.request
 *
 * @author jaymin
 * @since 2021/10/13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuiteTokenRequest implements Serializable {
    /**
     * 	以ww或wx开头应用id（对应于旧的以tj开头的套件id）
     */
    @JSONField(name = "suite_id")
    private String suiteId;
    /**
     * 	应用secret
     */
    @JSONField(name = "suite_secret")
    private String suiteSecret;
    /**
     * 	企业微信后台推送的ticket
     */
    @JSONField(name = "suite_ticket")
    private String suiteTicket;
}
