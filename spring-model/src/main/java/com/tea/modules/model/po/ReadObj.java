package com.tea.modules.model.po;

import com.tea.modules.enums.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * com.tea.modules.model.po
 *
 * @author jaymin
 * @since 2022/4/12
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReadObj {
    @DataType(type = Type.Horizontal)
    private Integer id;
}
