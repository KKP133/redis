package com.tea.modules.model.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * com.baturu.wechatwork.response
 *
 * @author jaymin
 * @since 2021/10/9
 */
@Data
public abstract class BaseResponse implements Serializable {
    /**
     * 出错返回码，为0表示成功，非0表示调用失败
     */
    @JSONField(name = "errcode")
    private int errCode;
    /**
     * 返回码提示语
     */
    @JSONField(name = "errmsg")
    private String errMsg;

    /**
     * 断言调用成功
     */
    public boolean assertSuccess() {
        return Objects.equals(0, this.errCode);
    }
}
