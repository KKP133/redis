package com.tea.modules.model.po;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

/**
 * com.tea.modules.model.po
 *每种原料都需要一些时间来准备。allOf() 等待所有的配料都准备好，然后使用更多些的时间将其混合成面糊。
 * 接下来，我们把单批面糊放入四个平底锅中烘烤。产品作为 CompletableFutures 流返回：
 * @author jaymin
 * @since 2022/4/24
 */
public class Baked {
    static class Pan {
    }

    static Pan pan(Batter b) {
        new Nap(0.1);
        return new Pan();
    }

    static Baked heat(Pan p) {
        new Nap(0.1);
        return new Baked();
    }

    static CompletableFuture<Baked> bake(CompletableFuture<Batter> cfb) {
        return cfb
                .thenApplyAsync(Baked::pan)
                .thenApplyAsync(Baked::heat);
    }

    public static Stream<CompletableFuture<Baked>> batch() {
        CompletableFuture<Batter> batter = Batter.mix();
        return Stream.of(
                bake(batter),
                bake(batter),
                bake(batter),
                bake(batter)
        );
    }
}
