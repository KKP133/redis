package com.tea.modules.model.po;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * com.tea.modules.model.po
 *
 * @author jaymin
 * @since 2021/7/23
 */
@Data
public class Device {
    private String type;
    @JsonProperty("p")
    private String property;
}
