package com.tea.modules.model.po;

import com.tea.modules.enums.Type;

import java.lang.annotation.*;

/**
 * @author jaymin
 * @create 2023-05-15 16:04
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.FIELD})
public @interface DataType {

    Type type() default Type.Horizontal;
}
