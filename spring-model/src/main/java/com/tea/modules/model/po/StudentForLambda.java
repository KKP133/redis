package com.tea.modules.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentForLambda {
    private Long id;
    private String name;
    private Integer age;
    private String address;
}
