package com.tea.modules.model.po;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * com.tea.modules.model.po
 *
 * @author jaymin
 * @since 2021/7/27
 */
@Data
public class UploadFileVO {
    private String sign;
    private List<MultipartFile> files;
}
