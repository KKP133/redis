package com.tea.modules.data.es.maaper;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import com.tea.modules.data.es.entity.EsDTO;

/**
 * com.tea.modules.data.es.maaper
 *
 * @author jaymin
 * @since 2022/8/25
 */
public interface MyMapper extends BaseEsMapper<EsDTO> {
}
