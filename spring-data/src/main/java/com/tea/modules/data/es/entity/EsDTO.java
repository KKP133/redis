package com.tea.modules.data.es.entity;

import cn.easyes.annotation.IndexId;
import cn.easyes.annotation.IndexName;
import cn.easyes.common.enums.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * com.tea.modules.data.es.entity
 *
 * @author jaymin
 * @since 2022/8/25
 */
@IndexName("")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EsDTO {
    @IndexId(type = IdType.CUSTOMIZE)
    private String id;
    /**
     * corpID
     */
    private String corpId;
    /**
     * 自动化营销id
     */
    private Long marketingAutomationId;
    /**
     * 客户id
     */
    private Long clientId;
    private String status;
    /**
     * 是否发送消息
     */
    private boolean sendMsg;
    /**
     * 是否点击
     */
    private boolean clickMsg;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Long createTime;
    /**
     * 更新时间
     */
    private Long updateTime;
}
