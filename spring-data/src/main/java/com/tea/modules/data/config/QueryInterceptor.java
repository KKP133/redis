package com.tea.modules.data.config;

import cn.easyes.annotation.Intercepts;
import cn.easyes.annotation.Signature;
import cn.easyes.core.conditions.LambdaEsQueryWrapper;
import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import cn.easyes.extension.context.Interceptor;
import cn.easyes.extension.context.Invocation;
import org.springframework.stereotype.Component;

/**
 * com.tea.modules.data.config
 *
 * @author jaymin
 * @since 2022/8/25
 */
@Intercepts(
        {
                @Signature(type = BaseEsMapper.class, method = "selectList", args = {LambdaEsQueryWrapper.class}),
        }
)
@Component
public class QueryInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("啊啊啊，我拦截到了查询，统一增加查询条件");
        // 查询条件中统一加入逻辑删除状态为未删除
        Object[] args = invocation.getArgs();
        LambdaEsQueryWrapper arg = (LambdaEsQueryWrapper) args[0];
        return invocation.proceed();
    }

}
